<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    $word1 = $faker->word;
    $word2 = $faker->word;

    return [
        'title' => "Fake Project {$word1} {$word2}",
        'slug' => "fake-project-{$word1}-{$word2}",
        'body' => 'This project does not exist.',
        'category_id' => function () {
            return factory(App\Category::class)->create()->id;
        },
    ];
});
