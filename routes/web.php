<?php
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth', 'prefix' => 'drafting', 'namespace' => 'Drafting'], function () {
    Route::get('projects', 'ProjectsController@index')->name('drafting.projects.index');
    Route::get('projects/new', 'ProjectsController@create')->name('drafting.projects.new');
    Route::post('projects', 'ProjectsController@store')->name('drafting.projects.store');
    Route::get('projects/{project}/edit', 'ProjectsController@edit')->name('drafting.projects.edit');
    Route::patch('projects/{project}', 'ProjectsController@update')->name('drafting.projects.update');
    Route::delete('projects/{project}', 'ProjectsController@destroy')->name('drafting.projects.delete');

    Route::delete('media/{media}', 'MediaController@destroy');
});

Route::get('/about', 'AboutController@show');

Route::get('{slug}', 'ProjectsController@show')->name('projects.show');
Route::get('/', 'ProjectsController@show');
