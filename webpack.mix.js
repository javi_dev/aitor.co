let mix = require("laravel-mix");
let tailwindcss = require("tailwindcss");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/assets/js/app.js", "public/js");

mix.postCss("resources/assets/css/main.css", "public/css", [
    tailwindcss("tailwind.js")
]);

mix.styles(
    [
        "public/css/main.css",
        "node_modules/medium-editor/dist/css/medium-editor.css",
        "node_modules/medium-editor/dist/css/themes/default.css"
    ],
    "public/css/main.css"
);
