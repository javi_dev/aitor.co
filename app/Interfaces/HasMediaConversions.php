<?php

namespace App\Interfaces;

use App\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

interface HasMediaConversions extends HasMedia
{
    public function registerMediaConversions(Media $media = null);
}
