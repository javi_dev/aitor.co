<?php

namespace App\Http\Controllers;

use App\Project;
use App\Category;

class AboutController extends Controller
{
    public function show()
    {
        return view('about.show', [
            'project' => new Project,
            'categories' => Category::all()
        ]);
    }
}
