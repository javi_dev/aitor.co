<?php

namespace App\Http\Controllers;

use App\Project;
use App\Category;

class ProjectsController extends Controller
{
    public function show($slug = 'ra-lates')
    {
        $project = Project::whereSlug($slug)->first();

        return view('projects.show', [
            'project' => $project,
            'categories' => Category::all(),
        ]);
    }
}
