<?php

namespace App\Http\Controllers\Drafting;

use App\Project;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ProjectsController extends Controller
{
    public function index()
    {
        $projects = Project::latest()->get();

        return view('drafting.projects.index', [
            'projects' => $projects,
        ]);
    }

    public function create()
    {
        return view('drafting.projects.new', [
            'project' => new Project,
        ]);
    }

    public function store()
    {
        request()->validate([
            'title' => ['required', Rule::unique('projects')],
            'category_id' => ['exists:categories,id'],
            'images[].*' => ['image'],
        ]);

        $project = Project::create([
            'title' => request('title'),
            'body' => request('body'),
            'slug' => str_slug(str_replace(':', '-', request('title'))),
            'category_id' => request('category_id'),
        ]);

        collect(request()->file('images'))->each(function ($image) use ($project) {
            $project->addImage($image);
        });

        return redirect(route('projects.show', $project->slug));
    }

    public function edit($id)
    {
        $project = Project::findOrFail($id);

        return view('drafting.projects.edit', [
            'project' => $project
        ]);
    }

    public function update($id)
    {
        request()->validate([
            'title' => ['required', Rule::unique('projects')->ignore($id)],
            'category_id' => ['exists:categories,id'],
            'images[].*' => ['image'],
        ]);

        $project = Project::findOrFail($id);

        $project->update([
            'title' => request('title'),
            'body' => request('body'),
            'slug' => str_slug(str_replace(':', '-', request('title'))),
            'category_id' => request('category_id'),
        ]);

        collect(request()->file('images'))->each(function ($image) use ($project) {
            $project->addImage($image);
        });

        return redirect()->route('drafting.projects.index');
    }

    public function destroy($id)
    {
        Project::findOrFail($id)->delete();

        return redirect()->route('drafting.projects.index');
    }
}
