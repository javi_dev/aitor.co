<?php

namespace App\Http\Controllers\Drafting;

use App\Media;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    public function destroy($id)
    {
        Media::find($id)->delete();

        return response()->json([], 204);
    }
}
