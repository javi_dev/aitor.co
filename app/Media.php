<?php

namespace App;

use Spatie\MediaLibrary\Media as BaseMedia;

class Media extends BaseMedia
{
    public function toArray()
    {
        return [
            'id' => $this->id,
            'url' => $this->getUrl(),
        ];
    }
}
