<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Project extends Model implements HasMediaConversions
{
    use HasMediaTrait;

    protected $guarded = [];

    protected $imagesCollection = 'project-images';

    public function registerMediaConversions(Media $media = null)
    {
        //
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getMediaPaths($collection = 'default')
    {
        return $this->getMedia($collection)->map(function ($media) {
            return "{$media->id}/{$media->file_name}";
        });
    }

    public function addImage($image, $filename = null)
    {
        if (is_null($filename)) {
            $filename = $image->getFilename();
        }

        $this
            ->addMedia($image->getPathname())
            ->preservingOriginal()
            ->usingFileName($filename)
            ->toMediaCollection($this->imagesCollection);
    }

    public function images()
    {
        return $this->getMedia($this->imagesCollection);
    }

    public function has_images()
    {
        return count($this->images()) > 0;
    }
}
