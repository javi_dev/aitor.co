import expect from "expect";
import { mount } from "vue-test-utils";
import DraftingUploader from "../../resources/assets/js/components/DraftingUploader.vue";

describe("DraftingUploader", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(DraftingUploader);
  });

  it("has a file input named 'images[]' with 'multiple'", () => {
    elementPropertyIs("input", "type", "file");
    elementPropertyIs("input", "name", "images[]");
    elementPropertyIs("input", "multiple", true);
  });

  it("shows images when selected", () => {
    wrapper.setData({
      images: [
        { id: 1, blob: new Blob(["image1"]) },
        { id: 2, blob: new Blob(["image2"]) }
      ]
    });

    expect(wrapper.findAll("img").length).toBe(2);
  });

  // Helper functions

  let see = (text, selector) => {
    let wrap = selector ? wrapper.find(selector) : wrapper;

    expect(wrap.html()).toContain(text);
  };

  let elementPropertyIs = (element, property, value) => {
    expect(wrapper.find(element).element[property]).toBe(value);
  };
});
