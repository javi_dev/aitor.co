import { mount } from "vue-test-utils";
import expect from "expect";
import Medium from "../../resources/assets/js/components/Medium.vue";

describe("Medium", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(Medium);
    });

    it("adds the medium-editor-element", () => {
        see("medium-editor-element");
    });

    it("creates a contenteditable", () => {
        see('contenteditable="true"');
    });

    it("a textarea has a name", () => {
        wrapper.setProps({ name: "Arya" });

        expect(wrapper.find('textarea[name="Arya"]').element.name).toBe("Arya");
    });

    it("a textarea has an id", () => {
        wrapper.setProps({ id: "girl" });

        expect(wrapper.find("#girl").element.id).toBe("girl");
    });

    it("a textarea has classes", () => {
        wrapper.setProps({
            classes: "bg-blue w-full"
        });

        expect(wrapper.find("textarea").element.className).toContain("bg-blue");
        expect(wrapper.find("textarea").element.className).toContain("w-full");
        // expect(wrapper.find("textarea").element.className).toContain(
        //     "medium-editor-hidden"
        // );
    });

    // Helper functions

    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;

        expect(wrap.html()).toContain(text);
    };
});
