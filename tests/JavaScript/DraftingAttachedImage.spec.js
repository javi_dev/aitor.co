import axios from "axios";
import td from "testdouble";
import expect from "expect";
import { mount } from "vue-test-utils";
import DraftingAttachedImage from "../../resources/assets/js/components/DraftingAttachedImage.vue";

describe("DraftingAttachedImage", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(DraftingAttachedImage, {
      propsData: {
        dataImg: {
          id: 1,
          url: "/path/to/image.png"
        }
      }
    });
  });

  afterEach(() => {
    td.reset();
  });

  it("shows the image", () => {
    elementPropertyIs("img", "src", "/path/to/image.png");
  });

  it("deletes the image on the server", () => {
    td.replace(axios, "delete");
    td.config({ ignoreWarnings: true }); // temporary
    td.when(axios.delete("/drafting/media/1")).thenResolve();

    wrapper.find("svg").trigger("click");

    td.verify(axios.delete("/drafting/media/1"));
  });

  it("deletes itself from the DOM when being deleted", async () => {
    td.replace(axios, "delete");
    td.when(axios.delete("/drafting/media/1")).thenResolve();

    wrapper.find("svg").trigger("click");

    await wrapper.vm.$nextTick();

    expect(wrapper.html()).not.toBeDefined();
  });

  // Helper functions

  let see = (text, selector) => {
    let wrap = selector ? wrapper.find(selector) : wrapper;

    expect(wrap.html()).toContain(text);
  };

  let elementPropertyIs = (element, property, value) => {
    expect(wrapper.find(element).element[property]).toBe(value);
  };
});
