import { mount } from "vue-test-utils";
import expect from "expect";
import ProjectsMenu from "../../resources/assets/js/components/ProjectsMenu.vue";

describe("ProjectsMenu", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(ProjectsMenu, {
      propsData: {
        dataProjectId: 20,

        dataCategories: [
          {
            id: 1,
            name: "/installation",
            projects: [
              {
                id: 19,
                title: "re:sound",
                slug: "re-sound",
                category_id: "1"
              },
              {
                id: 20,
                title: "ra lates",
                slug: "ra-lates",
                category_id: "1"
              },
              {
                id: 21,
                title: "graduation show",
                slug: "graduation-show",
                category_id: "1"
              }
            ]
          },
          {
            id: 2,
            name: "/product design",
            projects: [
              {
                id: 22,
                title: "trophies",
                slug: "trophies",
                category_id: "2"
              },
              {
                id: 23,
                title: "linbox",
                slug: "linbox",
                category_id: "2"
              },
              {
                id: 24,
                title: "1st class",
                slug: "1st-class",
                category_id: "2"
              }
            ]
          },
          {
            id: 3,
            name: "/make",
            projects: [
              {
                id: 25,
                title: "concrete",
                slug: "concrete",
                category_id: "3"
              },
              {
                id: 26,
                title: "paint",
                slug: "paint",
                category_id: "3"
              }
            ]
          }
        ]
      }
    });
  });

  it("shows the menu nav", () => {
    see("nav");
  });

  it("shows the category names", () => {
    see("/installation");
    see("/product design");
    see("/make");
  });

  it("shows the projects titles", () => {
    see("re:sound");
    see("ra lates");
    see("graduation show");
    see("trophies");
    see("linbox");
    see("1st class");
    see("concrete");
    see("paint");
  });

  it("shows the projects slugs", () => {
    see("re-sound");
    see("ra-lates");
    see("graduation-show");
    see("trophies");
    see("linbox");
    see("1st-class");
    see("concrete");
    see("paint");
  });

  it("paints a green background behind the selected project", () => {
    let a = wrapper.find('a[href="ra-lates"]');

    expect(a.classes()).toContain("bg-green-lighter");
  });

  it("paints a green background behind the about page link when visiting", () => {
    wrapper = mount(ProjectsMenu, {
      propsData: {
        dataProjectId: ""
      }
    });
    let a = wrapper.find('a[href="/about"]');

    expect(a.classes()).toContain("bg-green-lighter");
  });

  // TODO: hover

  // Helper functions

  let see = (text, selector) => {
    let wrap = selector ? wrapper.find(selector) : wrapper;

    expect(wrap.html()).toContain(text);
  };
});
