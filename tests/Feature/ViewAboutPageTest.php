<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewAboutPageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_guest_can_view_the_about_page()
    {
        $response = $this->get('/about');

        $response->assertStatus(200);
        $response->assertSee('img');
        $response->assertSee('Aitor');
    }
}
