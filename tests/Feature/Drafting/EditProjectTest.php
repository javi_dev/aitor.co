<?php

namespace Tests\Feature\Drafting;

use App\User;
use App\Project;
use App\Category;
use Tests\TestCase;
use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EditProjectTest extends TestCase
{
    use RefreshDatabase;

    private function validParams($overrides = [])
    {
        $overrides['category_id'] = $overrides['category_id'] ?? factory(Category::class)->create()->id;

        return array_merge([
            'title' => 'New title',
            'body' => 'New body',
        ], $overrides);
    }

    /** @test */
    function designer_can_view_the_edit_form_for_an_existing_project()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create();

        $response = $this->actingAs($user)->get(route('drafting.projects.edit', $project));

        $response->assertStatus(200);
        $response->assertViewIs('drafting.projects.edit');
        $this->assertTrue($response->data('project')->is($project));
    }

    /** @test */
    function designer_sees_a_404_when_attempting_to_view_the_edit_form_for_a_project_that_does_not_exist()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get("/drafting/projects/999/edit");

        $response->assertStatus(404);
    }

    /** @test */
    function guests_are_asked_to_login_when_attempting_to_view_the_edit_form_for_any_project()
    {
        $project = factory(Project::class)->create();

        $response = $this->get(route('drafting.projects.edit', $project));

        $response->assertRedirect('/login');
    }

    /** @test */
    function guests_are_asked_to_login_when_attempting_to_view_the_edit_form_for_a_project_that_does_not_exist()
    {
        $response = $this->get("/drafting/projects/999/edit");

        $response->assertRedirect('/login');
    }

    /** @test */
    function designer_can_edit_projects()
    {
        $user = factory(User::class)->create();
        $oldCategory = factory(Category::class)->create();
        $newCategory = factory(Category::class)->create();
        $project = factory(Project::class)->create([
            'title' => 'Old title',
            'body' => 'Old body',
            'category_id' => $oldCategory->id,
        ]);

        $response = $this->actingAs($user)->patch(route('drafting.projects.update', $project), [
            'title' => 'New title',
            'body' => 'New body',
            'category_id' => $newCategory->id,
        ]);

        $response->assertRedirect('/drafting/projects');
        tap($project->fresh(), function ($project) use ($newCategory) {
            $this->assertEquals('New title', $project->title);
            $this->assertEquals('New body', $project->body);
            $this->assertEquals($newCategory->id, $project->category_id);
        });
    }

    /** @test */
    function slugs_get_updated_when_editing_titles()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create([
            'title' => 'Old title',
            'slug' => 'old-title',
        ]);

        $response = $this->actingAs($user)->patch(route('drafting.projects.update', $project), $this->validParams([
            'title' => 'New title',
        ]));

        $response->assertRedirect('/drafting/projects');
        $this->assertEquals('new-title', $project->fresh()->slug);
    }

    /** @test */
    function designer_can_delete_project_images()
    {
        Storage::fake('public');
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create();
        $imageA = File::image('project-image-a.png');
        $imageToBeDeleted = File::image('image-to-be-deleted.png');
        $imageC = File::image('project-image-c.png');
        $project->addImage($imageA);
        $project->addImage($imageToBeDeleted);
        $project->addImage($imageC);
        $projectImageBefore = $project->images()->where('name', '==', $imageToBeDeleted->getFileName())->first();
        Storage::disk('public')->assertExists("{$projectImageBefore->id}/{$projectImageBefore->file_name}");

        $response = $this->actingAs($user)->deleteJson("/drafting/media/{$projectImageBefore->id}");

        $response->assertStatus(204);
        tap($project->fresh()->images(), function ($images) use ($imageA, $imageToBeDeleted, $imageC) {
            $this->assertContains($imageA->getFilename(), $images->pluck('file_name'));
            $this->assertNotContains($imageToBeDeleted->getFilename(), $images->pluck('file_name'));
            $this->assertContains($imageC->getFilename(), $images->pluck('file_name'));
        });
        Storage::disk('public')->assertMissing("{$projectImageBefore->id}/{$projectImageBefore->file_name}");
    }

    /** @test */
    function guests_get_a_404_when_trying_to_delete_images_that_exist()
    {
        Storage::fake('public');
        $project = factory(Project::class)->create();
        $image = File::image('project-image.png');
        $project->addImage($image, 'still-here.png');

        $response = $this->deleteJson("/drafting/media/{$project->images()->first()->id}");

        $response->assertStatus(404);
        $this->assertContains('still-here.png', $project->fresh()->images()->pluck('file_name'));
    }

    /** @test */
    function guests_get_a_404_when_tryin_to_delete_images_that_dont_exist()
    {
        $response = $this->deleteJson("/drafting/media/999");

        $response->assertStatus(404);
    }

    /** @test */
    function designer_can_add_images_to_a_project_being_edited()
    {
        Storage::fake('public');
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create();
        $existingImage = File::image('project-existing-image.png');
        $project->addImage($existingImage);
        $imageB = File::image('project-image-b.png');
        $imageC = File::image('project-image-b.png');

        $response = $this->actingAs($user)->patch(route('drafting.projects.update', $project), $this->validParams([
            'images' => [$imageB, $imageC],
        ]));

        tap(Project::first(), function ($project) use ($response, $existingImage, $imageB, $imageC) {
            $this->assertCount(3, $project->getMedia('project-images'));

            $this->assertFileOnDisk($project->getMediaPaths('project-images')[0]);
            $this->assertFileOnDisk($project->getMediaPaths('project-images')[1]);
            $this->assertFileOnDisk($project->getMediaPaths('project-images')[2]);

            $this->assertFileOnDiskEquals($existingImage, $project->getMediaPaths('project-images')[0]);
            $this->assertFileOnDiskEquals($imageB, $project->getMediaPaths('project-images')[1]);
            $this->assertFileOnDiskEquals($imageC, $project->getMediaPaths('project-images')[2]);
        });
    }


    /** @test */
    function designer_can_delete_projects()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create();

        $response = $this->actingAs($user)->delete(route('drafting.projects.delete', $project));

        $response->assertRedirect(route('drafting.projects.index'));
        $this->assertNull(Project::find($project->id));
    }

    /** @test */
    function guests_cannot_edit_projects()
    {
        $project = factory(Project::class)->create([
            'title' => 'Old title',
            'body' => 'Old body',
            'category_id' => 1,
        ]);

        $response = $this->patch(route('drafting.projects.update', $project), [
            'title' => 'New title',
            'body' => 'New body',
            'category_id' => 9,
        ]);

        $response->assertRedirect('/login');
        tap($project->fresh(), function ($project) {
            $this->assertEquals('Old title', $project->title);
            $this->assertEquals('Old body', $project->body);
            $this->assertEquals(1, $project->category_id);
        });
    }

    /** @test */
    function guests_cannot_delete_projects()
    {
        $project = factory(Project::class)->create();

        $response = $this->delete(route('drafting.projects.delete', $project));

        $response->assertRedirect('/login');
        $this->assertNotNull(Project::find($project->id));
    }

    /** @test */
    function title_is_required()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create([
            'title' => 'Old title',
        ]);

        $response = $this->actingAs($user)->from(route('drafting.projects.edit', $project))->patch(route('drafting.projects.update', $project), $this->validParams([
            'title' => '',
        ]));

        $response->assertRedirect("/drafting/projects/{$project->id}/edit");
        $response->assertSessionHasErrors('title');
        $this->assertEquals('Old title', $project->fresh()->title);
    }

    /** @test */
    function title_must_be_unique()
    {
        $user = factory(User::class)->create();
        $projectA = factory(Project::class)->create([
            'title' => 'Must Be Unique',
        ]);
        $projectB = factory(Project::class)->create([
            'title' => 'A Different Title',
        ]);

        $response = $this->actingAs($user)->from(route('drafting.projects.edit', $projectB))->patch(route('drafting.projects.update', $projectB), $this->validParams([
            'title' => 'Must Be Unique',
        ]));

        $response->assertRedirect("/drafting/projects/{$projectB->id}/edit");
        $response->assertSessionHasErrors('title');
        $this->assertEquals('A Different Title', $projectB->fresh()->title);
    }

    /** @test */
    function title_may_not_change_when_editing_other_fields()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create([
            'title' => 'May Remain',
            'body' => 'Old Body',
        ]);

        $response = $this->actingAs($user)->from(route('drafting.projects.edit', $project))->patch(route('drafting.projects.update', $project), $this->validParams([
            'title' => 'May Remain',
            'body' => 'Changed Body',
        ]));

        $response->assertRedirect("/drafting/projects");
        $this->assertEquals('May Remain', $project->fresh()->title);
    }

    /** @test */
    function body_is_optional()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create([
            'body' => 'Old body',
        ]);

        $response = $this->actingAs($user)->from(route('drafting.projects.edit', $project))->patch(route('drafting.projects.update', $project), $this->validParams([
            'body' => '',
        ]));

        $response->assertRedirect("/drafting/projects");
        $this->assertNull($project->fresh()->body);
    }

    /** @test */
    function category_must_exist()
    {
        $user = factory(User::class)->create();
        $category = factory(Category::class)->create();
        $project = factory(Project::class)->create([
            'category_id' => $category->id,
        ]);

        $response = $this->actingAs($user)->from(route('drafting.projects.edit', $project))->patch(route('drafting.projects.update', $project), $this->validParams([
            'category_id' => 999,
        ]));

        $response->assertRedirect("/drafting/projects/{$project->id}/edit");
        $response->assertSessionHasErrors('category_id');
        $this->assertTrue($project->fresh()->category->is($category));
    }

    /** @test */
    function project_image_must_be_an_image()
    {
        Storage::fake('public');
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create();
        $file = File::create('not-an-image.pdf');

        $response = $this->actingAs($user)->from(route('drafting.projects.edit', $project))->patch(route('drafting.projects.update', $project), $this->validParams([
            'images[]' => [$file],
        ]));

        $response->assertRedirect("/drafting/projects/{$project->id}/edit");
        $response->assertSessionHasErrors('images[].0');
        $this->assertCount(0, $project->fresh()->images());
    }

    /** @test */
    function designer_cannot_delete_a_project_that_doesnt_exist()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create();

        $response = $this->actingAs($user)->delete(route('drafting.projects.delete', 999));

        $response->assertStatus(404);
        $this->assertNotNull(Project::find($project->id));
    }

    /** @test */
    function designer_cannot_edit_a_project_that_doesnt_exist()
    {
        $user = factory(User::class)->create();
        $oldCategory = factory(Category::class)->create();
        $newCategory = factory(Category::class)->create();
        $project = factory(Project::class)->create([
            'title' => 'Old title',
            'body' => 'Old body',
            'category_id' => $oldCategory->id,
        ]);

        $response = $this->actingAs($user)->patch(route('drafting.projects.update', 999), [
            'title' => 'New title',
            'body' => 'New body',
            'category_id' => $newCategory->id,
        ]);

        $response->assertStatus(404);

        tap($project->fresh(), function ($project) use ($oldCategory) {
            $this->assertEquals('Old title', $project->title);
            $this->assertEquals('Old body', $project->body);
            $this->assertEquals($oldCategory->id, $project->category_id);
        });
    }
}
