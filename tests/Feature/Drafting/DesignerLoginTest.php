<?php

namespace Tests\Feature\Drafting;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DesignerLoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function logging_in_with_valid_credentials()
    {
        $user = factory(User::class)->create([
            'email' => 'designer@designer.co',
            'password' => bcrypt('super-secret-password'),
        ]);

        $response = $this->from('/login')->post('login', [
            'email' => 'designer@designer.co',
            'password' => 'super-secret-password',
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/drafting/projects');
        $this->assertTrue(Auth::check());
        $this->assertTrue(Auth::user()->is($user));
    }

    /** @test */
    function logging_in_with_invalid_credentials()
    {
        $user = factory(User::class)->create([
            'email' => 'designer@designer.co',
            'password' => bcrypt('super-secret-password'),
        ]);

        $response = $this->from('/login')->post('login', [
            'email' => 'designer@designer.co',
            'password' => 'not-the-right-password',
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/login');
        $this->assertFalse(Auth::check());
    }

    /** @test */
    function logging_in_with_an_account_that_doesnt_exist()
    {
        $user = factory(User::class)->create([
            'email' => 'designer@designer.co',
            'password' => bcrypt('super-secret-password'),
        ]);

        $response = $this->from('/login')->post('login', [
            'email' => 'nobody@nowhere.com',
            'password' => 'any-password',
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/login');
        $this->assertFalse(Auth::check());
    }

    /** @test */
    function logging_out_the_current_user()
    {
        Auth::login($user = factory(User::class)->create());
        $this->assertTrue(Auth::check());

        $response = $this->actingAs($user)->post('/logout');

        $response->assertRedirect('/login');
        $this->assertFalse(Auth::check());
    }
}
