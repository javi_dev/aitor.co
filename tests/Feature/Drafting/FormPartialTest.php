<?php

namespace Tests\Feature\Drafting;

use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FormPartialTest extends TestCase
{
    use RefreshDatabase;

    protected function assertViewHas($viewName, $key)
    {
        $factory = view();
        $view = $factory->make($viewName);
        $factory->callComposer($view);

        $this->assertArrayHasKey($key, $view->getData(), "Failed asserting that the view '{$viewName}' has the key '{$key}'");
    }

    protected function assertViewKeyIs($expected, $viewName, $key)
    {
        $factory = view();
        $view = $factory->make($viewName);
        $factory->callComposer($view);

        $expected->assertEquals($view->getData()[$key]);
    }

    /** @test */
    function view_should_have_all_categories()
    {
        $categoryA = factory(Category::class)->create();
        $categoryB = factory(Category::class)->create();

        $this->assertViewHas('drafting.projects._form', 'categories');
        $this->assertViewKeyIs(collect([$categoryA, $categoryB]), 'drafting.projects._form', 'categories');
    }
}
