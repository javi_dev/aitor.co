<?php

namespace Tests\Feature\Drafting;

use App\User;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewProjectListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function guests_cannot_view_the_designers_project_list()
    {
        $response = $this->get(route('drafting.projects.index'));

        $response->assertRedirect('/login');
    }

    /** @test */
    function designer_can_view_the_project_list_in_reverse_chronological_order()
    {
        $user = factory(User::class)->create();
        $projectA = factory(Project::class)->create([
            'created_at' => Carbon::parse('1 month ago'),
        ]);
        $projectB = factory(Project::class)->create([
            'created_at' => Carbon::parse('1 day ago'),
        ]);
        $projectC = factory(Project::class)->create([
            'created_at' => Carbon::parse('1 week ago'),
        ]);

        $response = $this->actingAs($user)->get(route('drafting.projects.index'));

        $response->assertStatus(200);
        $response->assertViewIs('drafting.projects.index');
        $response->data('projects')->assertEquals([
            $projectB,
            $projectC,
            $projectA,
        ]);
    }
}
