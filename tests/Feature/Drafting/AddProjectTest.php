<?php

namespace Tests\Feature\Drafting;

use App\User;
use App\Project;
use App\Category;
use Tests\TestCase;
use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddProjectTest extends TestCase
{
    use RefreshDatabase;

    private function validParams($overrides = [])
    {
        $overrides['category_id'] = $overrides['category_id'] ?? factory(Category::class)->create()->id;

        return array_merge([
            'title' => "Aitor's Awesome Project",
            'body' => 'This was a fun project. I learned a lot.',
        ], $overrides);
    }

    /** @test */
    function designer_can_view_the_add_concert_form()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/drafting/projects/new');

        $response->assertStatus(200);
        $response->assertViewIs('drafting.projects.new');
    }

    /** @test */
    function guests_cannot_view_the_add_concert_form()
    {
        $response = $this->get('/drafting/projects/new');

        $response->assertRedirect('/login');
    }

    /** @test */
    function adding_a_valid_easy_project()
    {
        $user = factory(User::class)->create();
        $category = factory(Category::class)->create();

        $response = $this->actingAs($user)->post(route('drafting.projects.store'), [
            'title' => "Easy Project",
            'body' => 'This was a fun project. I learned a lot.',
            'category_id' => $category->id,
        ]);

        tap(Project::first(), function ($project) use ($response, $category) {
            $response->assertRedirect($project->slug);
            $this->assertEquals('Easy Project', $project->title);
            $this->assertEquals('This was a fun project. I learned a lot.', $project->body);
            $this->assertEquals('easy-project', $project->slug);
            $this->assertTrue($category->is($project->category));
        });
    }

    /** @test */
    function guests_cannot_add_new_projects()
    {
        $category = factory(Category::class)->create();

        $response = $this->post(route('drafting.projects.store'), [
            'title' => "Easy Project",
            'body' => 'This was a fun project. I learned a lot.',
            'category_id' => $category->id,
        ]);

        $response->assertRedirect('/login');
        $this->assertCount(0, Project::all());
    }

    /** @test */
    public function adding_a_project_using_a_title_with_a_colon()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->post(route('drafting.projects.store'), $this->validParams([
            'title' => "re:sound",
        ]));

        $this->assertEquals('re-sound', Project::first()->slug);
    }

    /** @test */
    function adding_a_project_with_images()
    {
        Storage::fake('public');
        $user = factory(User::class)->create();
        $imageA = File::image('project-image-a.png');
        $imageB = File::image('project-image-b.png');
        $imageC = File::image('project-image-c.png');

        $response = $this->actingAs($user)->post(route('drafting.projects.store'), $this->validParams([
            'images' => [$imageA, $imageB, $imageC],
        ]));

        tap(Project::first(), function ($project) use ($response, $imageA, $imageB, $imageC) {
            $this->assertCount(3, $project->getMedia('project-images'));

            $this->assertFileOnDisk($project->getMediaPaths('project-images')[0]);
            $this->assertFileOnDisk($project->getMediaPaths('project-images')[1]);
            $this->assertFileOnDisk($project->getMediaPaths('project-images')[2]);

            $this->assertFileOnDiskEquals($imageA, $project->getMediaPaths('project-images')[0]);
            $this->assertFileOnDiskEquals($imageB, $project->getMediaPaths('project-images')[1]);
            $this->assertFileOnDiskEquals($imageC, $project->getMediaPaths('project-images')[2]);
        });
    }

    /** @test */
    function title_is_required()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->from('drafting.projects.new')->post(route('drafting.projects.store'), $this->validParams([
            'title' => '',
        ]));

        $response->assertRedirect('drafting.projects.new');
        $response->assertSessionHasErrors('title');
        $this->assertCount(0, Project::all());
    }

    /** @test */
    function title_must_be_unique()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create([
            'title' => 'Must Be Unique'
        ]);

        $response = $this->actingAs($user)->from('drafting.projects.new')->post(route('drafting.projects.store'), $this->validParams([
            'title' => 'Must Be Unique',
        ]));

        $response->assertRedirect('drafting.projects.new');
        $response->assertSessionHasErrors('title');
        $this->assertCount(1, Project::all());
    }

    /** @test */
    function body_is_optional()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->from('drafting.projects.new')->post(route('drafting.projects.store'), $this->validParams([
            'body' => '',
        ]));

        tap(Project::first(), function ($project) use ($response) {
            $response->assertRedirect($project->slug);
            $this->assertNull($project->body);
        });
    }

    /** @test */
    function category_must_exist()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->from('drafting.projects.new')->post(route('drafting.projects.store'), $this->validParams([
            'category_id' => 999,
        ]));

        $response->assertRedirect('drafting.projects.new');
        $response->assertSessionHasErrors('category_id');
        $this->assertCount(0, Project::all());
    }

    /** @test */
    function project_image_must_be_an_image()
    {
        Storage::fake('public');
        $user = factory(User::class)->create();
        $file = File::create('not-an-image.pdf');

        $response = $this->actingAs($user)->from('drafting.projects.new')->post(route('drafting.projects.store'), $this->validParams([
            'images[]' => [$file],
        ]));

        $response->assertRedirect('drafting.projects.new');
        $response->assertSessionHasErrors('images[].0');
        $this->assertCount(0, Project::all());
    }
}
