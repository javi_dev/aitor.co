<?php

namespace Tests\Feature;

use App\Project;
use App\Category;
use Tests\TestCase;
use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewProjectPageTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_guest_can_view_the_project_page()
    {
        Storage::fake('public');
        $title = "Aitor's awesome project";
        $project = factory(Project::class)->create([
            'title' => $title,
            'body' => 'This was a fun project. I learned a lot.',
            'slug' => str_slug($title)
        ]);

        $imageA = File::image('project-image-a.png');
        $imageB = File::image('project-image-b.png');
        $imageC = File::image('project-image-c.png');
        $project->addImage($imageA);
        $project->addImage($imageB);
        $project->addImage($imageC);

        $response = $this->get($project->slug);

        $response->assertStatus(200);
        $response->assertSee(e("Aitor's awesome project"));
        $response->assertSee('This was a fun project. I learned a lot.');
        $project->getMedia()->each(function ($media) use ($response) {
            $response->assertSee($media->getUrl());
        });
    }

    /** @test */
    function the_sidebar_menu_is_made_of_categories_and_links_to_projects()
    {
        $categoryA = factory(Category::class)->create([
            'name' => '/category a',
        ]);
        $categoryB = factory(Category::class)->create([
            'name' => '/category b',
        ]);
        $projectA = factory(Project::class)->create([
            'slug' => str_slug('project:a'),
            'category_id' => $categoryA->id,
        ]);

        $response = $this->get($projectA->slug);

        $response->assertStatus(200);
        $response->data('categories')->assertEquals([
            $categoryA,
            $categoryB,
        ]);
    }

    /** @test */
    function a_gest_can_view_the_defaul_project_page()
    {
        $project = factory(Project::class)->create([
            'slug' => 'ra-lates'
        ]);

        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
