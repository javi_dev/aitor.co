<?php

namespace Tests\Unit;

use App\Project;
use Tests\TestCase;
use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_returns_the_full_media_paths()
    {
        Storage::fake('public');
        $project = factory(Project::class)->create();
        $imageA = File::image('project-image-a.png');
        $imageB = File::image('project-image-b.png');
        $project->addImage($imageA, 'test1.png');
        $project->addImage($imageB, 'test2.png');

        $this->assertContains("test1.png", $project->images()->pluck('file_name'));
        $this->assertContains("test2.png", $project->images()->pluck('file_name'));
    }

    /** @test */
    function adding_images_to_a_project_associates_them_with_the_project()
    {
        Storage::fake('public');
        $project = factory(Project::class)->create();
        $imageA = File::image('project-image-a.png');
        $imageB = File::image('project-image-b.png');
        $project->addImage($imageA, 'test1.png');
        $project->addImage($imageB, 'test2.png');

        $this->assertCount(2, $project->images());
        $this->assertContains('test1.png', $project->images()->pluck('file_name'));
        $this->assertContains('test2.png', $project->images()->pluck('file_name'));
    }

    /** @test */
    function it_checks_availability_of_images()
    {
        Storage::fake('public');
        $project = factory(Project::class)->create();
        $this->assertFalse($project->has_images());

        $imageA = File::image('project-image-a.png');
        $imageB = File::image('project-image-b.png');

        $project->addImage($imageA, 'test1.png');
        $project->addImage($imageB, 'test2.png');

        $this->assertTrue($project->fresh()->has_images());
    }
}
