<?php

namespace Tests\Unit;

use App\Project;
use Tests\TestCase;
use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MediaTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function converting_to_array()
    {
        Storage::fake();
        $project = factory(Project::class)->create();
        $image = File::image('project-image.png');

        $project->addImage($image);
        $projectImage = $project->images()->first();

        $this->assertEquals([
            'id' => 1,
            'url' => $projectImage->getUrl()
        ], $projectImage->toArray());
    }
}
