<?php

namespace Tests\Browser\Drafting\Projects;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class NewTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    function form_button_should_say_Create_on_projects_new_page()
    {
        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit(route('drafting.projects.new'))
                    ->assertPathIs('/drafting/projects/new')
                    ->assertSeeIn('button', 'Create');
        });
    }
}
