<?php

namespace Tests\Browser\Drafting\Projects;

use App\User;
use App\Project;
use App\Category;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EditTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    function form_button_should_say_Edit_on_projects_edit_page()
    {
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create();

        $this->browse(function (Browser $browser) use ($user, $project) {
            $browser->loginAs($user)
                    ->visit(route('drafting.projects.edit', $project))
                    ->assertPathIs('/drafting/projects/1/edit')
                    ->assertSeeIn('button', 'Edit');
        });
    }

    /** @test */
    function form_displays_old_values()
    {
        $user = factory(User::class)->create();
        $categoryA = factory(Category::class)->create();
        $category = factory(Category::class)->create([
            'id' => 6,
        ]);
        $project = factory(Project::class)->create([
            'title' => 'Old Title',
            'category_id' => 6,
            'body' => 'Old Body',
        ]);

        $this->browse(function (Browser $browser) use ($user, $project, $categoryA) {
            $browser->loginAs($user)
                    ->visit(route('drafting.projects.edit', $project))
                    ->assertInputValue('title', 'Old Title')
                    ->assertSelectHasOptions('category_id', [$categoryA->id, 6])
                    ->assertSelected('category_id', 6);

            $this->assertEquals('Old Body', trim($browser->inputValue('body')));
        });
    }
}
