<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DesignerLoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    function logging_in_with_invalid_credentials()
    {
        $user = factory(User::class)->create([
            'email' => 'designer@designer.co',
            'password' => bcrypt('super-secret-password'),
        ]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('email', 'designer@designer.co')
                    ->type('password', 'wrong-password')
                    ->press('Sign in')
                    ->assertPathIs('/login')
                    ->assertSee('credentials do not match');
        });
    }

    /** @test */
    public function logging_in_successfully()
    {
        $user = factory(User::class)->create([
            'email' => 'designer@designer.co',
            'password' => bcrypt('super-secret-password'),
        ]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('email', 'designer@designer.co')
                    ->type('password', 'super-secret-password')
                    ->press('Sign in')
                    ->assertPathIs('/drafting/projects');
        });
    }

    /** @test */
    function logging_in_again()
    {
        $user = factory(User::class)->create([
            'email' => 'designer@designer.co',
            'password' => bcrypt('super-secret-password'),
        ]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertPathIs('/drafting/projects');
        });
    }
}
