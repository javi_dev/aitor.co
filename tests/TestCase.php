<?php

namespace Tests;

use PHPUnit\Framework\Assert;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp()
    {
        parent::setUp();

        EloquentCollection::macro('assertContains', function ($item) {
            Assert::assertTrue($this->contains($item), 'Failed asserting that the collection contains the specified item.');
        });

        EloquentCollection::macro('assertDoesNotContain', function ($item) {
            Assert::assertFalse($this->contains($item), 'Failed asserting that the collection does not contain the specified item.');
        });

        EloquentCollection::macro('assertEquals', function ($expected) {
            Assert::assertEquals(count($this), count($expected), 'Failed asserting that the collections have the same length.');

            $this->zip($expected)->each(function ($pair) {
                list($a, $b) = $pair;
                Assert::assertTrue($a->is($b), 'Failed asserting that the collections have the same items.');
            });
        });

        TestResponse::macro('data', function ($key) {
            return $this->original->getData()[$key];
        });
    }

    protected function assertFileOnDisk($path, $disk = 'public')
    {
        return Storage::disk($disk)->assertExists($path);
    }

    protected function assertFileOnDiskEquals($localFile, $uploadedFilePath, $disk = 'public')
    {
        return $this->assertFileEquals($localFile->getPathname(), Storage::disk($disk)->path($uploadedFilePath));
    }
}
