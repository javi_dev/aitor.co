<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', 'aitor.co')</title>

        <link rel="stylesheet" href="{{ elixir('css/main.css') }}">
    </head>
    <body class="bg-grey-lighter text-grey-dark font-sans">
        <div id="app">
            <header class="fixed ml-8 py-8">
                <a href="/" class="no-underline text-2xl md:text-3xl text-grey-darker">AITOR F.H. // spatial designer</a>
            </header>

            <projects-menu data-project-id="{{ $project->id }}" :data-categories="{{ $categories->load('projects') }}"></projects-menu>

            <section id="content" class="z-0 flex flex-col min-h-screen mx-8 md:ml-56">
                @yield('body')
            </section>
        </div>

        @stack('beforeScripts')
        <script src="{{ elixir('js/app.js') }}"></script>
        @stack('afterScripts')
    </body>
</html>
