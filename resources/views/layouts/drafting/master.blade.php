<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', 'aitor.co')</title>

        <link rel="stylesheet" href="{{ elixir('css/main.css') }}">
    </head>
    <body class="bg-grey-lighter h-screen font-sans hero-background">
        <div id="app" class="@yield('appClass')">
            @yield('body')
        </div>

        @stack('beforeScripts')
        <script src="{{ elixir('js/app.js') }}"></script>
        @stack('afterScripts')
    </body>
</html>
