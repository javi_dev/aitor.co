<ul class="list-reset">
    @foreach($categories as $category)
    <li class="text-grey-darker font-bold mb-6"><span>{{ $category-> name }}</span>
        @foreach($category->projects as $menu_project)
        <ul class="list-reset">
            <li class="font-normal">
                <a href="{{ $menu_project->slug }}"
                    class="no-underline text-grey-dark hover:bg-yellow {{ ! $menu_project->is($project) ?: "bg-green-lighter" }}">
                    {{ $menu_project->title }}
                </a>
            </li>
        </ul>
        @endforeach
    @endforeach
    <li><a href="/about" class="no-underline text-grey-dark hover:underline {{ $project->title ?: "bg-green-lighter" }}">about</a></li>
</ul>
