@extends('layouts.master')

@section('body')
    <img src="{{ '/img/aitor.jpg' }}" alt="Aitor" class="mt-24 self-start mb-4 w-full md:w-auto">
    <p>Hello, my name is <strong>Aitor</strong></p>
    <p>I live, work and study in London</p>
    <p>Receiving messages at hello at aitor dot co makes me happy</p>
@endsection
