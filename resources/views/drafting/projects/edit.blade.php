@extends('layouts.drafting.master')

@section('appClass', 'md:h-full')

@section('body')
<div class="container mx-auto bg-grey-lightest border-l border-r h-full flex flex-col">
    <h1 class="font-medium text-grey-darker py-4 px-8 border-b shadow">Edit Project {{ $project->title }}</h1>

    <form action="/drafting/projects/{{ $project->id }}" method="POST" class="flex flex-col flex-grow min-h-0" enctype="multipart/form-data">
        {{ method_field('PATCH') }}

        @include('drafting.projects._form', [
            'submitButtonText' => 'Edit Project'
        ])
    </form>
</div>
@endsection
