{{ csrf_field() }}
<div class="pt-8 pb-2 px-8 flex-none flex flex-wrap -mx-2">
    <div class="px-2 flex-grow md:w-3/4">
        <label for="title" class="label mb-2">Title</label>
        <input type="text" id="title" name="title" value="{{ $project->title }}" class="w-full rounded border py-2 px-3 shadow mb-2" placeholder="Title of the project" required>

        <div class="h-6">
            @if ($errors->has('title'))
            <p class="text-red italic">{{ $errors->get('title')[0] }}</p>
            @endif
        </div>
    </div>
    <div class="px-2">
        <label for="category_id" class="label mb-2">Category</label>
        <div class="relative">
            <select name="category_id" id="category_id" class="block appearance-none bg-white w-full px-4 py-2 pr-8 rounded border shadow">
                @foreach($categories as $category)
                <option value="{{ $category->id }}" {{ $category->id == $project->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
                @endforeach
            </select>
            <div class="pointer-events-none absolute pin-y pin-r flex items-center px-2 text-slate">
                <svg class="h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
            </div>
        </div>
    </div>
</div>

<div class="border-b pt-2 pb-6 px-8 flex-grow flex flex-col min-h-10">
    <label for="body" class="label mb-2">Body</label>
    <medium id="body" name="body" value="{{ $project->body }}" classes="w-full rounded border py-2 px-3 shadow overflow-auto" wrapper-classes="flex-grow flex min-h-0"></medium>
</div>

@if ($project->has_images())
<div class="px-8 py-4 border-b shadow-inner flex-none flex items-center">
        <p class="bold text-grey-dark text-xl mr-3 w-48">Already attached</p>
        <div class="flex overflow-hidden overflow-x-scroll">
            @foreach ($project->images() as $image)
                <drafting-attached-image :data-img="{{ $image }}"></drafting-attached-image>
            @endforeach
        </div>
</div>
@endif

<div class="py-4 px-8 border-b shadow-inner flex-none flex items-center">
    <p class="bold text-grey-dark text-xl w-48">{{ $project->has_images() ? 'Attach more images' : 'Attach images' }}</p>
    <drafting-uploader></drafting-uploader>
</div>

<div class="p-6 flex-none flex justify-center">
    <button type="submit" class="w-full md:w-1/3 lg:w-1/4 text-2xl py-4 bg-teal-dark font-medium text-teal-lightest rounded shadow-md hover:bg-teal-darker">{{ $submitButtonText ?? 'Create New Project' }}</button>
</div>
