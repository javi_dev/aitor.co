@extends('layouts.drafting.master')

@section('appClass', 'md:h-full')

@section('body')
<div class="container mx-auto bg-grey-lightest border-l border-r h-full flex flex-col">
    <h1 class="font-medium text-grey-darker py-4 px-8 border-b shadow">Add a New Project</h1>

    <form action="/drafting/projects" method="POST" class="flex-1 flex flex-col min-h-0" enctype="multipart/form-data">
        @include('drafting.projects._form')
    </form>
</div>
@endsection
