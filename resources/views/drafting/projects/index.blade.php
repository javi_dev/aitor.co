@extends('layouts.drafting.master')

@section('body')
<div class="container mx-auto">
    <div class="flex justify-center py-8">
        <a href="{{ route('drafting.projects.new') }}" class="no-underline text-center w-full md:w-1/3 lg:w-1/4 text-2xl py-4 bg-teal-dark font-medium text-teal-lightest rounded shadow-md hover:bg-teal-darker">Add a new project</a>
    </div>

    <div class="bg-grey-lightest border rounded shadow-md -mx-4 flex flex-wrap">
        @foreach($projects as $project)
            <div class="w-full md:w-1/4 max-w-sm p-4">
                <div class="h-full rounded shadow-lg relative">
                    <div class="h-64 mb-2 overflow-hidden">
                        <img class="w-full" src="{{ $project->getFirstMediaUrl('project-images') }}">
                    </div>
                    <div class="px-6 py-4 mb-6">
                        <div class="font-bold text-xl mb-2"><a href="{{ route('drafting.projects.edit', $project) }}">{{ $project->title }}</a></div>
                        <p class="text-grey-darker text-base">{!! str_limit($project->body, 100) !!}</p>
                    </div>
                    <form action="/drafting/projects/{{ $project->id }}" class="absolute pin-b pin-r p-4" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="text-grey">Delete</button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
