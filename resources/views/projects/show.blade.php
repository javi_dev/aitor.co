@extends('layouts.master')

@section('body')
    <div class="mt-24 text-xs leading-normal text-justify md:text-left md:w-104">
        {!! $project->body !!}
    </div>

    <div class="flex flex-wrap md:flex-no-wrap items-baseline -mx-3">
        @foreach ($project->images() as $media)
        <div class="flex-none p-3 w-full md:w-auto">
          <img src="{{ $media->getUrl() }}" alt="" class="w-full md:w-auto">
        </div>
        @endforeach
    </div>
@endsection

@push('afterScripts')
<script>
    document.addEventListener("wheel", function(e) {
      e.preventDefault();

      let delta = Math.sign(e.deltaY) * 40;

      document.documentElement.scrollLeft += delta;
    });
</script>
@endpush
