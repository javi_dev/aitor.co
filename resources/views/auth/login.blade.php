@extends('layouts.drafting.master')

@section('appClass', 'h-full')

@section('body')
<div class="container mx-auto h-full flex justify-center items-center">
    <div class="w-full md:w-1/2">

        <h1 class="text-center font-hairline mb-6">¡Hola Aitor!</h1>

        <form action="/login" method="POST" class="flex flex-col bg-grey-lightest rounded-lg shadow-lg p-8 border-teal border-t-8">
            {{ csrf_field() }}
            <div class="mb-4">
                <label for="email" class="label mb-2">email</label>
                <input type="email" id="email" name="email" value="{{ old('email') }}" placeholder="Your email" class="appearance-none w-full py-2 px-3 text-grey-dark rounded border shadow-md" autofocus>
            </div>

            <div class="mb-4">
                <label for="password" class="label mb-2">Password</label>
                <input type="password" id="password" name="password" placeholder="Your Password" class="appearance-none w-full py-2 px-3 text-grey-dark rounded border shadow-md">
            </div>

            <div class="mb-4 h-6 text-red text-sm italic">
                @if ($errors->any())
                <p class="text-red italic">{{ $errors->first() }}</p>
                @endif
            </div>

            <button type="submit" class="w-full md:w-3/4 self-center py-4 bg-teal text-teal-lightest font-bold text-xl hover:bg-teal-dark rounded-lg shadow">Sign in</button>
        </form>
    </div>
</div>
@endsection
